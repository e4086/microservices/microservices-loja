package com.estudo.fornecedor.service;

import com.estudo.fornecedor.model.InfoFornecedor.InfoFornecedor;
import com.estudo.fornecedor.repository.InfoFornecedorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InfoService {

    private static final Logger LOG = LoggerFactory.getLogger(InfoService.class);

    @Autowired
    private InfoFornecedorRepository infoFornecedorRepository;

    public InfoFornecedor buscarInfoEstado(String estado) {
        LOG.info("informações buscadas");
        return infoFornecedorRepository.findByEstado(estado);
    }
}
