package com.estudo.fornecedor.repository;

import com.estudo.fornecedor.model.InfoFornecedor.InfoFornecedor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfoFornecedorRepository extends JpaRepository<InfoFornecedor, Long> {

    InfoFornecedor findByEstado(String estado);
}
