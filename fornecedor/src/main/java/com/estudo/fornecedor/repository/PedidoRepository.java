package com.estudo.fornecedor.repository;

import org.springframework.data.repository.CrudRepository;

import com.estudo.fornecedor.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Long>{

}
