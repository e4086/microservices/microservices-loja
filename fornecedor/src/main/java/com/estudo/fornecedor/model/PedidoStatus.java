package com.estudo.fornecedor.model;

public enum PedidoStatus {
	RECEBIDO, PRONTO, ENVIADO;
}
