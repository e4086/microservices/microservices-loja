package com.estudo.fornecedor.model.InfoFornecedor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "tb_info_fornecedor")
public class InfoFornecedor {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "if_id")
    private Long id;

    @Column(name = "if_nome")
    private String nome;

    @Column(name = "if_estado")
    private String estado;

    @Column(name = "if_endereco")
    private String endereco;
}
