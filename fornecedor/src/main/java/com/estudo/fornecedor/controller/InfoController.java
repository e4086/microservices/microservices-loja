package com.estudo.fornecedor.controller;

import com.estudo.fornecedor.model.InfoFornecedor.InfoFornecedor;
import com.estudo.fornecedor.service.InfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/info")
public class InfoController {

    private static final Logger LOG = LoggerFactory.getLogger(InfoController.class);

    @Autowired
    private InfoService infoService;

    @GetMapping("/{estado}")
    public InfoFornecedor buscarInfoEstado(@PathVariable String estado) {
        LOG.info("recebido pedido de informaçoes do fornecedor {}", estado);
        return infoService.buscarInfoEstado(estado);
    }
}
