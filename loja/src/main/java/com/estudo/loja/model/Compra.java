package com.estudo.loja.model;


import com.estudo.loja.enumaration.CompraStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "tb_compra")
public class Compra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "co_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "co_status")
    private CompraStatus status;

    @Column(name = "pe_id")
    @EqualsAndHashCode.Include
    private Long pedidoId;

    @Column(name = "co_tempo_de_preparo")
    private Integer tempoDePreparo;

    @Column(name = "co_endereco_destino")
    private String enderecoDestino;

    @Column(name = "co_data_para_entrega")
    private LocalDate dataParaEntrega;

    @Column(name = "en_id")
    private Long voucher;
}
