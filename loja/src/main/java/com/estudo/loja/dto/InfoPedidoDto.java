package com.estudo.loja.dto;

import lombok.Data;

@Data
public class InfoPedidoDto {
    private Long id;

    private Integer tempoDePreparo;
}
