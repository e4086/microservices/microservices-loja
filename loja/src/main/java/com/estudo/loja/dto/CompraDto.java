package com.estudo.loja.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class CompraDto {

    @JsonIgnore
    private Long id;

    private List<ItemCompraDto> itens;

    private EnderecoDto endereco;
}
