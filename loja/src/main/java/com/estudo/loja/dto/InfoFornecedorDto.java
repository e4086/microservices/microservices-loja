package com.estudo.loja.dto;

import lombok.Data;

@Data
public class InfoFornecedorDto {
    private String endereco;
}
