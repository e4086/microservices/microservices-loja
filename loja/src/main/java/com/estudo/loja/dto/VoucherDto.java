package com.estudo.loja.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class VoucherDto {

    private Long numero;

    private LocalDate previsaoParaEntrega;

}
