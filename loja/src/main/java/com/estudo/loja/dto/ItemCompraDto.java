package com.estudo.loja.dto;


import lombok.Data;

@Data
public class ItemCompraDto {
    private Long id;
    private int quantidade;
}
