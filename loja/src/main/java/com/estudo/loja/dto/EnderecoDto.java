package com.estudo.loja.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class EnderecoDto {
    private String rua;
    private String numero;
    private String estado;
}
