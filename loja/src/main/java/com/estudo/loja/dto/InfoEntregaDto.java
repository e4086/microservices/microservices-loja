package com.estudo.loja.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class InfoEntregaDto {
    private Long pedidoId;

    private LocalDate dataParaEntrega;

    private String enderecoOrigem;

    private String enderecoDestino;

}
