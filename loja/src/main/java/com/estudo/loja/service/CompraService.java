package com.estudo.loja.service;

import com.estudo.loja.client.FornecedorClient;
import com.estudo.loja.client.TransportadorClient;
import com.estudo.loja.dto.CompraDto;
import com.estudo.loja.dto.InfoEntregaDto;
import com.estudo.loja.dto.InfoFornecedorDto;
import com.estudo.loja.dto.InfoPedidoDto;
import com.estudo.loja.dto.VoucherDto;
import com.estudo.loja.enumaration.CompraStatus;
import com.estudo.loja.model.Compra;
import com.estudo.loja.repository.CompraRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static java.util.Objects.nonNull;

@Service
public class CompraService {

    private static final Logger LOG = LoggerFactory.getLogger(CompraService.class);

    @Autowired
    private FornecedorClient fornecedorClient;

    @Autowired
    private TransportadorClient transportadorClient;

    @Autowired
    private CompraRepository compraRepository;


    @HystrixCommand(threadPoolKey = "buscarPorIdPool")
    public Compra buscarPorId(Long id) {
        return compraRepository.findById(id).orElse(new Compra());
    }

    @HystrixCommand(fallbackMethod = "realizarCompraFallback", threadPoolKey = "realizarCompraPool")
    public Compra realizarCompra(CompraDto compraDto) {
        String enderecoDestino = compraDto.getEndereco().toString();

        Compra compraSalva = new Compra();
        compraSalva.setStatus(CompraStatus.RECEBIDO);
        compraSalva.setEnderecoDestino(enderecoDestino);
        compraRepository.save(compraSalva);

        compraDto.setId(compraSalva.getId());

        String estado = compraDto.getEndereco().getEstado();

        LOG.info("Buscando informações do Fornecedor de {}", estado);
        InfoFornecedorDto infoFornecedorDto = fornecedorClient.buscarInfoEstado(estado);

        LOG.info("Realizando pedido", estado);
        InfoPedidoDto pedidoDto = fornecedorClient.realizarPedido(compraDto.getItens());

        Integer tempoDePreparo = pedidoDto.getTempoDePreparo();

        compraSalva.setStatus(CompraStatus.PEDIDO_REALIZADO);
        compraSalva.setPedidoId(pedidoDto.getId());
        compraSalva.setTempoDePreparo(tempoDePreparo);
        compraRepository.save(compraSalva);

        InfoEntregaDto entregaDTO = new InfoEntregaDto();
        entregaDTO.setPedidoId(pedidoDto.getId());
        entregaDTO.setDataParaEntrega(LocalDate.now().plusDays(tempoDePreparo));
        entregaDTO.setEnderecoOrigem(infoFornecedorDto.getEndereco());
        entregaDTO.setEnderecoDestino(enderecoDestino);

        VoucherDto voucherDto = transportadorClient.reservaEntrega(entregaDTO);

        compraSalva.setStatus(CompraStatus.RESERVA_ENTREGA_REALIZADA);
        compraSalva.setDataParaEntrega(voucherDto.getPrevisaoParaEntrega());
        compraSalva.setVoucher(voucherDto.getNumero());
        compraRepository.save(compraSalva);

        return compraSalva;
    }

    public Compra realizarCompraFallback(CompraDto compraDto) {

         if(nonNull(compraDto.getId())){
            return compraRepository.findById(compraDto.getId()).get();
        }

        Compra compraFallback = new Compra();
        compraFallback.setEnderecoDestino(compraDto.getEndereco().toString());
        return compraFallback;
    }

}
