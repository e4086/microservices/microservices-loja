package com.estudo.loja.client;

import com.estudo.loja.dto.InfoFornecedorDto;
import com.estudo.loja.dto.InfoPedidoDto;
import com.estudo.loja.dto.ItemCompraDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("fornecedor")
public interface FornecedorClient {

    @RequestMapping("/info/{estado}")
    InfoFornecedorDto buscarInfoEstado(@PathVariable String estado);

    @RequestMapping(value = "/pedido", method = RequestMethod.POST)
    InfoPedidoDto realizarPedido(List<ItemCompraDto> itens);
}