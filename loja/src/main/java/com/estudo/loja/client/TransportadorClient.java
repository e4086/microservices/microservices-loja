package com.estudo.loja.client;

import com.estudo.loja.dto.InfoEntregaDto;
import com.estudo.loja.dto.VoucherDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("transportador")
public interface TransportadorClient {

    @PostMapping("/entrega")
    VoucherDto reservaEntrega(@RequestBody InfoEntregaDto pedidoDTO);

}
