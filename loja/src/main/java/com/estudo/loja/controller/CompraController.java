package com.estudo.loja.controller;

import com.estudo.loja.dto.CompraDto;
import com.estudo.loja.model.Compra;
import com.estudo.loja.service.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/compra")
public class CompraController {

    @Autowired
    private CompraService compraService;

    @GetMapping("/{id}")
    public Compra buscarCompra(@PathVariable Long id){
        return compraService.buscarPorId(id);
    }

    @PostMapping
    public Compra comprar(@RequestBody CompraDto compraDto) {
        return compraService.realizarCompra(compraDto);
    }
}
