package com.estudo.loja.enumaration;

public enum CompraStatus {
    RECEBIDO,
    PEDIDO_REALIZADO,
    RESERVA_ENTREGA_REALIZADA;
}
