package com.estudo.transportador.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_entrega")
public class Entrega {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	@Column(name = "en_id")
	private Long id;

	@Column(name = "pe_id")
	private Long pedidoId;

	@Column(name = "en_data_para_busca")
	private LocalDate dataParaBusca;

	@Column(name = "en_previsao_para_entrega")
	private LocalDate previsaoParaEntrega;

	@Column(name = "en_endereco_origem")
	private String enderecoOrigem;

	@Column(name = "en_endereco_destino")
	private String enderecoDestino;
}
